﻿using System;
using SolutisBank.Bank;

namespace SolutisBank
{
    class Program
    {
        static void Main(string[] args)
        {

            ContaCorrente contaCorrente = new ContaCorrente();
            Conta conta0 = new ContaCorrente();

            // ContaCorrente conta1 = new Conta();
            // Conta conta1 = new Conta();

            conta0.Agencia = new Agencia("REDENCAO", 125, new Banco("BRADESCO"));

            conta0.Correntista = new Correntista("Paloma");
            conta0.Numero = "233";

            conta0.Depositar(15.01m);
            Console.WriteLine("Saldo ===> " + conta0.Saldo);

            conta0.Depositar(20.01m);
            Console.WriteLine("Saldo ===> " + conta0.Saldo);

            conta0.Sacar(22.03m);
            Console.WriteLine("Saldo ===> " + conta0.Saldo);

            // conta0.Sacar(13.00);
            conta0.Sacar(12.99m);
            Console.WriteLine("Saldo ===> " + conta0.Saldo);

            // Teste o software com nunit e resolva o problema da precisão;


        }
    }
}
