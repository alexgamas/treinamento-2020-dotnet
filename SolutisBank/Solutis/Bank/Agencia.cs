using System;

namespace SolutisBank.Bank
{
    public class Agencia
    {
        public string Nome { get; set; }
        public int Numero { get; set; }
        public Banco Banco { get; }
        public DateTime DataCriacao { get; }

        public Agencia()
        {
            this.DataCriacao = DateTime.Now;
        }

        public Agencia(string nome, int numero) : this()
        {
            this.Nome = nome;
            this.Numero = numero;
            Console.WriteLine("Agencia Criada " + DataCriacao 
                + " com o numero: " + this.Numero + " Nome: " + this.Nome);
        }

        public Agencia(string nome, int numero, Banco banco) : this(nome, numero)
        {
            this.Banco = banco;
            Console.WriteLine("Agencia Criada " + DataCriacao 
                + " com o numero: " + this.Numero + " Nome: " + this.Nome + " Com banco");
        }
    }
}