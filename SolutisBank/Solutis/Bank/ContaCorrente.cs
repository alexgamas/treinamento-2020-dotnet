using System;

namespace SolutisBank.Bank
{
    public class ContaCorrente : Conta
    {
        public Correntista Correntista { get; set; }
        public Agencia Agencia { get; set; }
        public string Numero { get; set; }
        public decimal Saldo { get; set; }

        public void Depositar(decimal valor)
        {
            this.Saldo += valor;
        }

        public void Sacar(decimal valor)
        {
            // "If I had a dime for every time I've seen someone use FLOAT to store currency, I'd have $999.997634"
            //                                                                                -- Cerqueira, Emanuel
            if (Saldo >= valor)
            {
                this.Saldo -= valor;
                // return;
            }
            else
            {
                throw new Exception("saldo insuficiente!!");
            }
        }
    }

}