namespace SolutisBank.Bank
{
    public class Correntista
    {
        public string Nome { get; set; }
        public Correntista(string nome)
        {
            this.Nome = nome;
        }
    }
}