namespace Solutis.Bank.Core
{
    public interface Conta
    {
        // Attributes - Composition
        Correntista Correntista { get; set; }
        Agencia Agencia { get; set; }

        string Numero { get; set; }
        decimal Saldo  { get; set; }
        
        // Methods
        void Depositar(decimal valor);
        void Sacar(decimal valor);
    }
}