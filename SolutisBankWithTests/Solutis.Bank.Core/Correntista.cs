namespace Solutis.Bank.Core
{
    public class Correntista
    {
        public string Nome { get; set; }
        public Correntista(string nome)
        {
            this.Nome = nome;
        }
    }
}