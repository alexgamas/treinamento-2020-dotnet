﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Solutis.Blog.Core.Domain.Entity;
using Solutis.Blog.Data.Config;

namespace Solutis.Blog.Controllers
{
    [ApiController]
    [Route("/api/post")]
    public class BlogPostController : ControllerBase
    {

        // public static List<Post> meusposts = new List<Post>();

        private DatabaseContext context;

        public BlogPostController(DatabaseContext context)
        {
            this.context = context;
        }    

        [HttpPost]
        public ActionResult<Post> NewPost([FromBody] PostRequest request)
        {

            // var id = meusposts.Count() + 1;
            // meusposts.Add(new Post
            // {
            //     Title = request.Title,
            //     Content = request.Content,
            //     Id = id
            // });
            // return Ok(id);

            Post post = new Post
            {
                Title = request.Title,
                Content = request.Content
            };

            context.Posts.Add(post);
            context.SaveChanges();

            return CreatedAtRoute("GetPostById", new { id = post.Id }, post);
        }

        // [HttpPost("batch")]
        // public ActionResult<List<long>> NewPosts([FromBody] List<PostRequest> request)
        // {
        //     if (request != null)
        //     {
        //         var first_id = meusposts.Count() + 1;
        //         long id = first_id;
        //         List<Post> posts = request.Select((request) =>
        //         {
        //             var post = new Post
        //             {
        //                 Title = request.Title,
        //                 Content = request.Content,
        //                 Id = id
        //             };
        //             id++;
        //             return post;
        //         }).ToList();
        //         meusposts.AddRange(posts);
        //         return Ok(posts.Select(p => p.Id).ToList());
        //     }
        //     return BadRequest("Erro ao adicionar posts");
        // }

        [HttpGet] // http://localhost:5000/api/post
        public ActionResult<List<Post>> GetPosts()
        {
            // if (meusposts == null || meusposts.Count() == 0)
            // {
            //     return NoContent();
            // }
            // return Ok(meusposts);

            return context.Posts.ToList();
        }

        /// <summary>
        /// Returns a single post given an Post <Id>
        /// </summary>
        /// <param name="Id">The Post Id.</param>
        /// <returns>A Single post.</returns>

        [HttpGet("{id}", Name = "GetPostById")] // http://localhost:5000/api/post/12
        public ActionResult<Post> GetPostById([FromRoute] long id)
        {
            // if (meusposts == null || meusposts.Count() == 0)
            // {
            //     return NoContent();
            // }
            // return Ok(meusposts);

            Post post = context.Posts.Where(p => p.Id == id).SingleOrDefault();

            if (post == null)
            {
                return NotFound("Post not found");
            }

            return Ok(post);
        }
    }
}
