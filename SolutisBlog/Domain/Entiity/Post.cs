using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solutis.Blog.Core.Domain.Entity
{
    [Table("blog_post")]
    public class Post
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("title")]
        [Required]
        public string Title { get; set; }

        [Column("content")]
        [Required]
        public string Content { get; set; }

        [Column("creation")]
        [Required]
        public DateTime CreationDate { get; set; }

        public Post()
        {
            this.CreationDate = DateTime.Now;
        }

    }

}