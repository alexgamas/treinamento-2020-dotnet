using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solutis.Blog.Core.Domain.Entity
{
    public class PostRequest
    {
        public string Title { get; set; }

        public string Content { get; set; }

    }

}