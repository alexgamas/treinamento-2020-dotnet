﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Solutis.Blog.Api.Controllers
{
    [ApiController]
    [Route("/")]
    public class HomeController : ControllerBase
    {

        [HttpGet]
        public ActionResult<Object> Get()
        {
   
            return Ok(new {
                Result = "Ok",
                Time = DateTime.Now
            });
            
        }
    }
}
