﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Solutis.Blog.Core.Domain.Entity;
using Solutis.Blog.Core.Services.Interfaces;

namespace Solutis.Blog.Controllers
{
    [ApiController]
    [Route("/api/post")]
    public class BlogPostController : ControllerBase
    {

        // public static List<Post> meusposts = new List<Post>();

        // private DatabaseContext context;
        private IPostService service;
        

        public BlogPostController(/*DatabaseContext context1,*/IPostService service)
        {
            // this.context = context;
            this.service = service;
        }    

        [HttpPost]
        public ActionResult<PostResponse> NewPost([FromBody] PostRequest request)
        {

            // var id = meusposts.Count() + 1;
            // meusposts.Add(new Post
            // {
            //     Title = request.Title,
            //     Content = request.Content,
            //     Id = id
            // });
            // return Ok(id);

            // ------------------------------------------------------

            // Post post = new Post
            // {
            //     Title = request.Title,
            //     Content = request.Content
            // };

            // context.Posts.Add(post);
            // context.SaveChanges();

            // return CreatedAtRoute("GetPostById", new { id = post.Id }, post);

            // ------------------------------------------------------

            Post newPost = new Post
            {
                Title = request.Title,
                Content = request.Content
            };

            Post post = service.Save(newPost);

            PostResponse postResponse = new PostResponse(post);

            return CreatedAtRoute("GetPostById", new { id = postResponse.Id }, postResponse);
            
        }

        // [HttpPost("batch")]
        // public ActionResult<List<long>> NewPosts([FromBody] List<PostRequest> request)
        // {
        //     if (request != null)
        //     {
        //         var first_id = meusposts.Count() + 1;
        //         long id = first_id;
        //         List<Post> posts = request.Select((request) =>
        //         {
        //             var post = new Post
        //             {
        //                 Title = request.Title,
        //                 Content = request.Content,
        //                 Id = id
        //             };
        //             id++;
        //             return post;
        //         }).ToList();
        //         meusposts.AddRange(posts);
        //         return Ok(posts.Select(p => p.Id).ToList());
        //     }
        //     return BadRequest("Erro ao adicionar posts");
        // }

        [HttpGet] // http://localhost:5000/api/post
        public ActionResult<List<PostResponse>> GetPosts()
        {
            // if (meusposts == null || meusposts.Count() == 0)
            // {
            //     return NoContent();
            // }
            // return Ok(meusposts);
            
            // ------------------------------------------------------
            
            // return context.Posts.ToList();

            // ------------------------------------------------------

            List<PostResponse> postResponses = service.GetAll().Select((post) => new PostResponse(post)).ToList();
            
            return Ok(postResponses);
        }

        /// <summary>
        /// Returns a single post given a Post Id <Id>
        /// </summary>
        /// <param name="Id">The Post Id.</param>
        /// <returns>A Single post.</returns>
        [HttpGet("{id}", Name = "GetPostById")] // http://localhost:5000/api/post/12
        public ActionResult<PostResponse> GetPostById([FromRoute] long id)
        {
            // if (meusposts == null || meusposts.Count() == 0)
            // {
            //     return NoContent();
            // }
            // return Ok(meusposts);

            // ------------------------------------------------------

            // Post post = context.Posts.Where(p => p.Id == id).SingleOrDefault();

            // if (post == null)
            // {
            //     return NotFound("Post not found");
            // }
            // return Ok(post);

            // ------------------------------------------------------

            Post post = service.GetOne(id);

            if (post == null)
            {
                return NotFound("Post not found");
            }

            return Ok(new PostResponse(post));
        }


        /// <summary>
        /// Deletes a single post given a Post Id <Id>
        /// [DELETE] http://localhost:5000/api/post/<Id>
        /// </summary>
        /// <param name="Id">The Post Id.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult DeletePostById([FromRoute] long id)
        {
            
            service.Remove(id);

            return NoContent();
        }


        /// <summary>
        /// Deletes a single post given a Post Id <Id>
        /// [DELETE] http://localhost:5000/api/post/<Id>
        /// </summary>
        /// <param name="Id">The Post Id.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public ActionResult<PostResponse> UpdatePost([FromRoute] long id, [FromBody] PostRequest request)
        {
            
            Post newPost = new Post
            {
                Id = id,
                Title = request.Title,
                Content = request.Content
            };

            Post p = service.Update(newPost);

            return Ok(new PostResponse(p));
        }


    }
}
