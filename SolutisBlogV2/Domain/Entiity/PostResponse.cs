using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Solutis.Blog.Core.Domain.Entity
{
    public class PostResponse
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreationDate { get; set; }
        public HashSet<string> Tags { get; set; }

        private HashSet<string> BuildTags(string tags)
        {
            if (!string.IsNullOrWhiteSpace(tags))
            {
                return tags.Split(",").Select((l) => l.Trim()).ToHashSet();
            }

            return new HashSet<string>();
        }

        public PostResponse() : base()
        {

        }

        public PostResponse(Post post) : this()
        {
            if (post != null)
            {
                this.Id = post.Id;
                this.Title = post.Title;
                this.Content = post.Content;
                this.CreationDate = post.CreationDate;
                this.Tags = BuildTags(post.Tags);
            }
        }

    }

}