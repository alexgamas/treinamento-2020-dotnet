using System.Collections.Generic;
using Solutis.Blog.Core.Domain.Entity;

namespace Solutis.Blog.Core.Interfaces
{
    public interface IPostRepository
    {
        List<Post> GetAll();
        Post GetOne(long id);
        Post Save(Post post);
        void Remove(Post post);
        Post Update(Post post);
    }
}