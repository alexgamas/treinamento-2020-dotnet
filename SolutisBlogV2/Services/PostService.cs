
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Web;
using Solutis.Blog.Core.Domain.Entity;
using Solutis.Blog.Core.Domain.Models;
using Solutis.Blog.Core.Interfaces;
using Solutis.Blog.Core.Services.Interfaces;

namespace Solutis.Blog.Core.Services
{
    public class PostService : IPostService
    {
        private readonly HttpClient client;
        private IPostRepository postRepository;
        private ICommentRepository commentRepository;

        public PostService(
            IPostRepository postRepository, 
            ICommentRepository commentRepository)
        {
            this.postRepository = postRepository;
            this.commentRepository = commentRepository;
            this.client = new HttpClient();
        }

        public List<Post> GetAll()
        {
            return postRepository.GetAll();
        }

        public Post GetOne(long id)
        {
            return postRepository.GetOne(id);
        }

        public long NumberOfComments(long postId)
        {
            return commentRepository.CountCommentsByPostId(postId);
        }

        public void Remove(long id)
        {
            Post post = GetOne(id);

            if (post == null)
            {
                throw new BusinessLogicException("Post not found");
            }

            postRepository.Remove(post);
        }

        public Post Save(Post post)
        {
            /************************************************************
            / Chamar a api para auto classificar o post e criar as tags
            ************************************************************/
            string key = Environment.GetEnvironmentVariable("API_AUTOTAG_KEY");
            string text = HttpUtility.UrlEncode($"{post.Title} {post.Content}");
            string url = $"https://api.meaningcloud.com/class-2.0?of=json&key={key}&txt={text}&model=IPTC_pt";

            string meaningTagsResponse = client.GetStringAsync(url).Result;
            /*
            {
                "status": {
                    "code": "0",
                    "msg": "OK",
                    "credits": "1",
                    "remaining_credits": "19978"
                },
                "category_list": [
                    {
                        "code": "14006000",
                        "label": "assuntos sociais - família",
                        "abs_relevance": "0.11258696",
                        "relevance": "100"
                    },
                    {
                        "code": "01010000",
                        "label": "arte, cultura e espetáculos - literatura",
                        "abs_relevance": "0.08907599",
                        "relevance": "79"
                    }
                ]
            }
            */

            MeaningResponse meaning = JsonSerializer.Deserialize<MeaningResponse>(meaningTagsResponse);

            post.Tags = meaning.GetTags();

            return postRepository.Save(post);
        }

        public Post Update(Post post)
        {
            Post postFound = GetOne(post.Id);

            if (post == null)
            {
                throw new BusinessLogicException("Post not found");
            }

            postFound.Title = post.Title;
            postFound.Content = post.Content;

            return postRepository.Update(postFound);

        }
    }
}