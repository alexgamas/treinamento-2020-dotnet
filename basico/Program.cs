﻿using System;
using System.Collections.Generic;
using System.Linq;
using SalaDeAula;
using Zoo;

namespace basico
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello turminha animada da noite!");

            var nome = "dotnet .....";

            Console.WriteLine(nome);

            nome = "Alex Gamas";

            Console.WriteLine(nome);

            String texto = $"O dado contido {{ na variável nome é {nome}";

            Console.WriteLine($"este é o texto '{texto}'");

            string meuNome = "Alex";

            int idade = 36;

            // double altura = 1.73;

            float altura = 1.73f;

            Console.WriteLine("Nome:   {0}", meuNome);
            Console.WriteLine("Idade:  {0}", idade);
            Console.WriteLine("Altura: {0}", altura);

            var numero = 0.003;

            Console.WriteLine(numero.GetType());
            Console.WriteLine(typeof(double));


            Console.WriteLine(numero.GetType() == typeof(double));

            Console.WriteLine(numero is double);

            bool IsDouble(object o)
            {
                return o.GetType() == typeof(double);
            }

            Console.WriteLine($" 12 é double? {IsDouble(12)}");
            Console.WriteLine($" 12.22 é double? {IsDouble(12.22)}");
            Console.WriteLine($" 12.22f é double? {IsDouble(12.22f)}");

            /**********************************************************************************************/
            // Precision

            // isso nao funciona
            // var nome2 = "vitor";
            // nome2 = 33;

            // float - 32 bit (7 digits)
            // double - 64 bit (15-16 digits)
            // decimal - 128 bit (28-29 significant digits)

            Console.WriteLine("--------------------------------------------");
            int distanciaInt = 13 / 7;
            float distanciaFloat = 13.0f / 7;
            double distanciaDouble = 13.0 / 7;
            decimal distanciaDecimal = 13.0m / 7;

            Console.WriteLine("distanciaInt {0}", distanciaInt);
            Console.WriteLine("distanciaFloat {0}", distanciaFloat);
            Console.WriteLine("distanciaDouble {0}", distanciaDouble);
            Console.WriteLine("distanciaDecimal {0}", distanciaDecimal);

            /**********************************************************************************************/
            // Type Cast

            Console.WriteLine("(int) distanciaDecimal {0}", (int)distanciaDecimal);
            Console.WriteLine("(float) distanciaDecimal {0}", (float)distanciaDecimal);
            Console.WriteLine("(double) distanciaDecimal {0}", (double)distanciaDecimal);

            decimal meuDecimalforcado = (decimal)((double)distanciaDecimal);

            Console.WriteLine("(decimal???) meuDecimalforcado {0}", (double)distanciaDecimal);

            // // Não adianta tentar aumentar a precisao na formataçao
            Console.WriteLine("distanciaInt {0:F4}", distanciaInt);


            /**********************************************************************************************/
            // Null

            // int? numeroQuePodeSerNulo = null;
            Nullable<int> numeroQuePodeSerNulo = null;

            Console.WriteLine($"Numero Tem Valor? {numeroQuePodeSerNulo.HasValue}");
            Console.WriteLine($"Numero: [{numeroQuePodeSerNulo}]");

            if (numeroQuePodeSerNulo.HasValue)
            {
                Console.WriteLine($"Numero Tem Valor");
            }
            else
            {
                Console.WriteLine($"Numero NAO Tem Valor");
            }

            numeroQuePodeSerNulo = 3;


            if (numeroQuePodeSerNulo.HasValue)
            {
                Console.WriteLine($"Numero Tem Valor e é {numeroQuePodeSerNulo.Value}");
            }
            else
            {
                Console.WriteLine($"Numero NAO Tem Valor");
            }

            Console.WriteLine($"Numero  Tem Valor? {numeroQuePodeSerNulo.HasValue}");
            Console.WriteLine($"Numero: {numeroQuePodeSerNulo}");
            Console.WriteLine($"Numero: {numeroQuePodeSerNulo.Value}");

            Dog cachorro = new Dog("rex");

            Cat gato = null;

            if (gato != null)
            {
                Console.WriteLine($"gato nao é nulo");
                Console.WriteLine($"Valor: {gato}");
            }
            else
            {
                Console.WriteLine($"gato é nulo");
            }

            gato = new Cat();
            gato.Nome = "possivel garfield";

            // var garfield = gato != null ? gato : new Cat();

            // ?? é um nulish coalescense operator
            var garfield = gato ?? new Cat();

            if (gato != null)
            {
                Console.WriteLine($"gato nao é nulo");
                Console.WriteLine($"Valor: {garfield}");
            }
            else
            {
                Console.WriteLine($"gato é nulo");
            }

            // Exercicio para descontrair;

            // A quantidade de calorias que se queima subindo um degrau é, em média, de 0,17 e 0,05 para descer. 
            // Sabemos que entre dois andares do prédio da solutis temos 20 degraus. 
            // Quantas calorias Você queima subindo e descendo estes degraus 3 vezes ao dia?

            // const int degrausPorAndar = 20;
            // const double caloriasSubirUmDegrau = 0.17;
            // const double caloriasDescerUmDegrau = 0.05;
            // const int numeroSubidas = 3;

            // double caloriasSubirUmAndar = degrausPorAndar * caloriasSubirUmDegrau;
            // double caloriasDescerUmAndar = degrausPorAndar * caloriasDescerUmDegrau;
            // double caloriasTotalAndar = caloriasSubirUmAndar + caloriasDescerUmAndar;
            // double caloriasTotal = caloriasTotalAndar * numeroSubidas;

            // Console.WriteLine("Calorias total: {0}", caloriasTotal);

            int[] array0 = new int[4];
            array0[0] = 1;
            array0[1] = 7;
            array0[2] = 8;
            array0[3] = 12;

            int[] array1 = new int[] { 1, 7, 8, 12 };
            int[] array2 = { 1, 7, 8, 12 };
            var array3 = new int[] { 1, 7, 8, 12 };

            // Não funciona
            // var array4 = { 1, 7, 8, 12 };
            for (int i = 0; i < array2.Length; i++)
            {
                Console.WriteLine("O valor na posicao {0} é {1}", i, array2[i]);
            }

            foreach (var item in array0)
            {
                Console.WriteLine("O valor é {0}", item);
            }

            /**********************************************************************************************/

            // Agora e se eu quiser saber o resultado de calorias para um numero variado de andares?

            const int degrausPorAndar = 20;
            const double caloriasSubirUmDegrau = 0.17;
            const double caloriasDescerUmDegrau = 0.05;
            const int numeroSubidas = 3;
            double caloriasSubirUmAndar = degrausPorAndar * caloriasSubirUmDegrau;
            double caloriasDescerUmAndar = degrausPorAndar * caloriasDescerUmDegrau;
            double caloriasTotalAndar = caloriasSubirUmAndar + caloriasDescerUmAndar;

            int[] andares = { 1, 7, 8, 12 };

            for (int i = 0; i < andares.Length; i++)
            {
                int nAndares = andares[i];

                double caloriasTotal = caloriasTotalAndar * numeroSubidas * nAndares;
                Console.WriteLine("para {0} andares o total decalorias é:  {1}", nAndares, caloriasTotal);
            }

            /**********************************************************************************************/

            List<int> lista0 = new List<int>();
            List<int> lista1 = new List<int> { 1, 3, 4 };

            var lista2 = new List<int>();
            var lista3 = new List<int> { 1, 3, 4 };

            foreach (var item in lista3)
            {
                Console.WriteLine("valor {0}", item);
            }

            Console.WriteLine("-----------------------------------------------");

            lista3.Add(12);
            lista3.Add(12);
            lista3.Add(24);
            lista3.Add(48);

            foreach (var item in lista3)
            {
                Console.WriteLine("+ADD+ valor {0}", item);
            }

            Console.WriteLine("-----------------------------------------------");

            lista3.Remove(12);

            foreach (var item in lista3)
            {
                Console.WriteLine("-Remove- valor {0}", item);
            }

            Console.WriteLine("-----------------------------------------------");

            // https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.hashset-1?view=netcore-3.1

            HashSet<int> hs0 = new HashSet<int>();
            var hs1 = new HashSet<int>();
            var hs2 = new HashSet<string>();
            var hs3 = new HashSet<Cat>();
            var hs4 = new HashSet<Dog>();
            var hs5 = new HashSet<Object>();

            // -----------------------------------------------

            // --> void
            hs5.Add(new Cat("Bixano"));
            hs5.Add(new Cat("Snow Flake"));
            hs5.Add(new Cat("Meouth"));
            hs5.Add(new Dog("Rex"));

            foreach (var item in hs5)
            {
                bool ehGato = (item is Cat);
                Console.WriteLine($" o objeto {item} é gato? {ehGato}");

                if (ehGato)
                {
                    string nomeGato = ((Cat)item).Nome;
                    Console.WriteLine($" o nome do gato é {nomeGato}");
                }
            }

            // --> new Cat("Bixano")
            // --> void

            Console.WriteLine("-----------------------------------------------");

            hs0.Add(1);
            hs0.Add(3);
            hs0.Add(4);
            hs0.Add(12);
            hs0.Add(12);
            hs0.Add(12);
            hs0.Add(12);
            hs0.Add(24);
            hs0.Add(48);

            foreach (var item in hs0)
            {
                Console.WriteLine("-Remove- valor {0}", item);
            }

            Console.WriteLine("-----------------------------------------------");

            SortedSet<int> ss0 = new SortedSet<int>();
            var ss1 = new SortedSet<int>();

            ss0.Add(12);
            ss0.Add(12);
            ss0.Add(12);
            ss0.Add(1);
            ss0.Add(48);
            ss0.Add(4);
            ss0.Add(12);
            ss0.Add(24);
            ss0.Add(3);

            foreach (var item in ss0)
            {
                Console.WriteLine("-Remove- valor {0}", item);
            }

            Console.WriteLine("-----------------------------------------------");

            Dictionary<int, string> dic0 = new Dictionary<int, string>();
            var dic1 = new Dictionary<int, string>();

            dic0.Add(1, "Lucas");
            dic0.Add(2, "Italo");
            dic0.Add(3, "Eduardo");
            dic0.Add(5, "Vinicius");
            dic0.Add(7, "Gabriel");
            dic0.Add(9, "Thiago");
            dic0.Add(10, "Thiago");

            int key_vh = 7;

            if (!dic0.ContainsKey(key_vh))
            {
                dic0.Add(key_vh, "Vitor Hugo");
            }

            key_vh++;
            dic0.Add(key_vh, "Vitor Hugo");

            foreach (var key in dic0.Keys)
            {
                string value = dic0[key];

                Console.WriteLine($"{key}\t{value}");
            }

            Console.WriteLine($"total de {dic0.Count} pessoas");

            Console.WriteLine("-----------------------------------------------");

            Cat thunderCat = new Cat("Shitara");
            thunderCat.dataNascimento = DateTime.Now.AddYears(-2);

            Console.WriteLine($"O gato {thunderCat.Nome} tem {thunderCat.IdadeDoGatoEmDias()} dias de idade");

            Console.WriteLine("---------- Funcao anonima ----------");

            int Quadrado0(int numero)
            {
                return numero * numero;
            }

            var oQuadradoDe4 = Quadrado0(4);

            Console.WriteLine(oQuadradoDe4);

            // https://docs.microsoft.com/en-us/dotnet/api/system.func-2?view=net-5.0
            Func<int, int> Quadrado1 = (numero) => numero * numero;

            var oQuadradoDe5 = Quadrado1(5);

            Console.WriteLine(oQuadradoDe5);

            Console.WriteLine("-----------------------------------------------");

            Cat pokeCat = new Cat("Meouth");

            Console.WriteLine(pokeCat.Nome);

            //funcao de sotaque
            string sotaque0(int i)
            {
                return "miau tchê";
            }

            Console.WriteLine(pokeCat.Falar(sotaque0));

            //funcao de sotaque
            Func<int, string> sotaque1 = (i) =>
            {
                return "miau meô";
            };

            Console.WriteLine(pokeCat.Falar(sotaque1));

            //funcao de sotaque
            Func<int, string> sotaque2 = (i) => "oxente miau!";

            Console.WriteLine(pokeCat.Falar(sotaque2));

            //funcao de sotaque
            Console.WriteLine(pokeCat.Falar((i) =>
            {
                return "miau uai!";
            }));

            //funcao de sotaque
            Console.WriteLine(pokeCat.Falar((i) => "miau sôr!"));


            Console.WriteLine("-----------------------------------------------");

            // Não façam isso em casa

            // HOF => Higher Order Function
            Func<int, int> TesteHOF(int valorInicial, Func<int, int> lambda)
            {
                int valorInternoInicial = lambda(valorInicial);
                return (x) => x * valorInternoInicial;
            }

            int resultado = TesteHOF(5, (i) => i * 2)(10);
            Console.WriteLine(resultado);

            // https://docs.microsoft.com/en-us/dotnet/api/system.action-1?view=netcore-3.1

            // mais bizarro
            Func<int, Func<int, int>, Func<int, int>> TesteHOF2 = (int valorInicial, Func<int, int> lambda) => 
            {
                int valorInternoInicial = lambda(valorInicial);
                return (x) => x * valorInternoInicial;
            };
        
            int resultadoHOF2 = TesteHOF2(5, (i) => i * 2)(10);
            Console.WriteLine(resultado);


            // Actions -> Lambda com retorno void
            void TestAction1(int p, Action<int> a)
            {
                a(p);
            }

            TestAction1(12, (b) => Console.WriteLine(b));

            new List<string>() { 
                "elemento1",
                "elemento2"
            }.ForEach((b) => Console.WriteLine(b));


            Action<string> exibe = (valor) => {
                Console.WriteLine(valor);
            };

            new List<string>() { 
                "elemento1",
                "elemento2"
            }.ForEach(exibe);

            // , Action/ só para descontrair;
            Func<int, Action> contar = (int inicial) => {
                int contador = inicial;

                return () => {
                    Console.WriteLine(contador);
                    contador += 1;
                };
            };

            var c = contar(12);
            c();
            c();
            c();

            Console.WriteLine("-----------------------------------------------");

            Aluno a = new Aluno("", 1);

            List<Aluno> alunos = new List<Aluno>();

            alunos.Add(new Aluno("Lucas", 22));
            alunos.Add(new Aluno("Italo", 25));
            alunos.Add(new Aluno("Eduardo", 15));
            alunos.Add(new Aluno("Vinicius", 31));
            alunos.Add(new Aluno("Gabriel", 18));
            alunos.Add(new Aluno("Thiago", 72));

            alunos.ForEach((aluno) => Console.WriteLine(aluno));

            alunos.ForEach((aluno) => {
                if (aluno.Idade >= 18)
                {
                    Console.WriteLine(aluno);
                }
            });


            List<Aluno> alunosMaioresDeIdade = alunos.Where((aluno) => aluno.Idade >= 18).ToList();

            alunosMaioresDeIdade.ForEach((aluno) => {
                Console.WriteLine(aluno);
            });


            List<string> nomeDosAlunos = alunos.Select((aluno) => aluno.Nome.ToUpper()).ToList();

            nomeDosAlunos.ForEach((aluno) => {
                Console.WriteLine(aluno);
            });

            List<string> nomeDosAlunosMaioresDeIdade = alunos
                .Where((aluno) => aluno.Idade >= 18)
                .Select((aluno) => aluno.Nome.ToUpper()).ToList();

            nomeDosAlunosMaioresDeIdade.ForEach((aluno) => {
                Console.WriteLine(aluno);
            });

            // 22
            // 25
            // >> 15 Fora
            // 31
            // 18
            // 72

            int somaDaIdadeDosAlunosMaioresDeIdade = alunos
                .Where((aluno) => aluno.Idade >= 18)
                .Select((aluno) => aluno.Idade)
                .Aggregate((a, b) => {
                    return a + b;
                });

            Console.WriteLine(somaDaIdadeDosAlunosMaioresDeIdade);


            int somaDaIdadeDosAlunosMaioresDeIdadeSomandoCom20Anos = alunos
                .Where((aluno) => aluno.Idade >= 18)
                .Select((aluno) => aluno.Idade)
                .Aggregate(20, (a, b) => {
                    return a + b;
                });

            Console.WriteLine(somaDaIdadeDosAlunosMaioresDeIdadeSomandoCom20Anos);


            List<string> animaisDaAmazonia = new List<string>()
            {
                "Anta (Tapirus terrestris)",
                "Araçari-castanho (Pteroglossus castanotis)",
                "Arara-canindé (Ara ararauna)",
                "Arara-vermelha (Ara chloropterus)",
                "Ariranha (Pteronura brasiliensis)",
                "Boto cor-de-rosa (Inia geoffrensis)",
                "Cachorro-vinagre (Speothos venaticus)",
                "Caititu ou porco-do-mato (Pecari tajacu)",
                "Cobra-cigarra ou jequitiranaboia (Fulgora sp.)",
                "Coró-coró (Mesembrinibis cayennensis)",
                "Garça-real (Pilherodius pileatus)",
                "Gavião-real (Harpia harpyja)",
                "Irara (Eira barbara)",
                "Jacaré-açu (Melanosuchus niger)",
                "Jacaretinga (Caiman crocodilus)",
                "Jararaca-cinza (Bothriopsis taeniata)",
                "Macaco-aranha-de-cara-branca (Ateles marginatus)",
                "Mariposa-atlas (Rothschildia sp)",
                "Onça-pintada (Panthera onca)",
                "Pavãozinho-do-Pará (Eurypyga helias)",
                "Peixe-boi-da-amazônia (Trichechus inunguis)",
                "Saí-andorinha (Tersina viridis)",
                "Sucuri (Eunectes murinus)",
                "Suçuarana (Puma concolor)",
                "Surucucu (Lachesis muta)"
            };

            // Desafio: Contar todas as letras dos nomes. Sem os Espaços e simbolos;
            var quantidade = animaisDaAmazonia
                .Select((i) => string.IsNullOrWhiteSpace(i) ? 0 : i.Count((c) => char.IsLetter(c)))
                .Aggregate((a, b) => a + b);

            Console.WriteLine("-----------------------------------------------");

            string nomeString = "Emanuel";

            Console.WriteLine(nomeString.Equals("Emanuel")); // Insecure

            Console.WriteLine("Emanuel".Equals(nomeString));

            Console.WriteLine(string.Equals(nomeString, "Emanuel", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
