
--------------------
--    drop all    --
--------------------

drop table if exists post_comment;
drop sequence if exists post_comment_id_seq;

drop table if exists blog_post;
drop sequence if exists blog_post_id_seq;

-----------------
--    posts    --
-----------------

create sequence blog_post_id_seq increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;

create table blog_post (
    id bigint default nextval('blog_post_id_seq') not null,
    title character varying(500) not null,
    content character varying(5000) not null,
    creation timestamp(4) without time zone not null,
    constraint post_pkey primary key (id)
);

--------------------
--    comments    --
--------------------

create sequence post_comment_id_seq increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;

create table post_comment (
    id bigint default nextval('post_comment_id_seq') not null,
    id_post bigint not null,
    content character varying(1000) not null,
    creation timestamp(4) without time zone not null,
    constraint comment_pkey primary key (id),
    constraint id_post_fk foreign key (id_post) references blog_post (id) match simple
);
