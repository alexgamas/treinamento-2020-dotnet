using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace models
{

    [Table("blog_post")]
    public class Post
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("creation")]
        public DateTime CreationDate { get; set; }

        public List<Comment> Comments { get; set; }

        public Post()
        {
            this.CreationDate = DateTime.Now;
        }
    }
}