using System;

namespace Solutis
{
    public class Pessoa
    {
        public string Nome { get; set; }
        public int Idade { get; set; }

        // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/operator-overloading

        public static Pessoa operator +(Pessoa p1, Pessoa p2)
        {
            Pessoa filho = new Pessoa();
            filho.Nome = "Filho de " + p1.Nome + " e " + p2.Nome;
            filho.Idade = 0;
            return filho;
        }

        public static Pessoa operator -(Pessoa p1, Pessoa p2)
        {
            throw new Exception("Seu monstro!!!!, vc não pode subtrair pessoas!!!");
        }

        public static bool operator ==(Pessoa p1, Pessoa p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(Pessoa p1, Pessoa p2)
        {
            return !p1.Equals(p2);
        }

        public override int GetHashCode()
        {
            return this.Nome.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Pessoa)
            {
                return String.Equals(
                    ((Pessoa)obj).Nome, this.Nome,
                    StringComparison.InvariantCultureIgnoreCase);
            };

            return base.Equals(obj);
        }

        public override string ToString()
        {
            return $"Nome: {this.Nome}\tIdade: {this.Idade}";
        }
    }
}